# An-Extension-of-the-Application-Class-in-Android
public class MyApplication extends Application
{
	//global variable
	private static final String mGlobalVariable;
	
	@Override
	public void onCreate()
	{
		super.onCreate();
		//... initialize global variables here
		myGlobalVariable = loadCacheData();
	}
	
	public static String getMyGlobalVariable() {
		return myGlobalVariable;
	}
	
}
